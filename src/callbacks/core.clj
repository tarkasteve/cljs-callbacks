(ns callbacks.core
  (:require [clojure.core.async :as async]))

(defn adder [a b f]
  (f (+ a b)))

(defn caller1 []
  (adder 1 1 (fn [x] (println x))))

(defn caller2 []
  (let [var (atom nil)]
    (adder 1 1 #(reset! var %))
    (println @var)))

(defn caller3 []
  (let [rchan (async/chan 1)]
    (adder 1 1 (fn [result] (async/>!! rchan result)))
    (println (async/<!! rchan))))


(defn caller4 []
  (println (async/<!! (let [rchan (async/chan 1)]
                        (adder 1 1 (fn [result] (async/>!! rchan result)))
                        rchan))))

(defn caller5 []
  (adder 1 1 (fn [x] (println x))))


(defn add3 []
  (->> (+ 1 2)
       (+ 3)
       (+ 4)))

(defn adder3 []
  (adder 1 2 (fn [r]
               (adder 3 r (fn [r]
                            (adder 4 r (fn [r] (println r))))))))

(defn adder3-c []
  (let [rchan (async/chan 1)]
    (adder 1 2 (fn [r]
                 (adder 3 r (fn [r]
                              (adder 4 r (fn [r]
                                           (async/>!! rchan r)))))))
    (async/<!! rchan)))


;; Credit for this `foldr` implemenation goes to Yoshinori Kohyama.
;; See https://gist.github.com/kohyama/2893987
(defn foldr [f s coll]
  ((reduce (fn [acc x] #(acc (f x %))) identity coll) s))  

(defmacro -|| [& body]
  (if body
    (let [chanvar (gensym)
          retvar (gensym)
          metadata {:chanvar chanvar
                    :retvar retvar}
          ;; Wrap 'bare' forms in a list, same as `->`
          nbody (map #(if (seq? %1) %1 (list %1)) body)

          ;; Callbackify the body forms:
          ;;
          ;;    (a b) (c d) => (a b r (fn [r] (c d))
          ;;
          ;; Treat the first separately as we it doesn't require an
          ;; injected second argument.
          threaded1 (foldr (fn [a b] (concat a [retvar `(fn [~retvar] ~b)]))
                        `(clojure.core.async/>!! ~chanvar ~retvar)
                        (rest nbody))
          threaded (concat (first nbody) [`(fn [~retvar] ~threaded1)])
          outform `(let [~chanvar (clojure.core.async/chan 1)]
                     ~threaded
                     (clojure.core.async/<!! ~chanvar))]

      ;; We append the gensym metadata to the generated code for
      ;; testing purposes.
      (with-meta outform metadata))))
