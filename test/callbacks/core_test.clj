(ns callbacks.core-test
  (:require [clojure.test :refer :all]
            [clojure.string :as string]
            [callbacks.core :refer :all]))

(deftest test-matching-structure
  (let [outform (macroexpand-1 `(-|| (adder 1 2)
                                     (adder 3)
                                     (adder 4)))
        {retvar  :retvar
         chanvar :chanvar} (meta outform)

        target `(let [~chanvar (clojure.core.async/chan 1)]
                  (adder 1 2 (fn [~retvar]
                               (adder 3 ~retvar (fn [~retvar]
                                            (adder 4 ~retvar (fn [~retvar]
                                                         (clojure.core.async/>!! ~chanvar ~retvar)))))))
                  (clojure.core.async/<!! ~chanvar))]

    (is (= outform target))))

(deftest test-3-adders
  (is (= 10
         (-|| (adder 1 2)
              (adder 3)
              (adder 4)))))

(deftest test-bare-form
  (let [a3 (partial adder 3)]
    (is (= 10
           (-|| (adder 1 2)
                a3
                (adder 4))))))

(deftest test-empty-macro
  (-||))

(deftest test-single-form
  (is (= 3
         (-|| (adder 1 2)))))
